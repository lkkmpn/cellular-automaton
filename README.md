# Elementary cellular automaton

A proof-of-concept of an
[elementary cellular automaton](https://en.wikipedia.org/wiki/Elementary_cellular_automaton)
in LaTeX and Ti*k*Z.

![Rule 110](automaton-110.svg)

## Usage

At the top of [automaton.tex](automaton.tex), a few configuration options are
available: the initial state, the rule to apply, the number of cells, the
number of generations (after the initial generation), and the size of the cells
in the output file.

Example output of rule 110 is shown above and included
[in PDF format](automaton-110.pdf) (svg shown above created using
[pdf2svg](https://github.com/dawbarton/pdf2svg) and compressed using
[SVGOMG](https://jakearchibald.github.io/svgomg/)).

## License

This project is licensed under [the MIT license.](LICENSE.md)
